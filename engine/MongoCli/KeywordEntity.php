<?php
namespace MongoCli;

use MongoDB\BSON\Binary;
use MongoDB\BSON\Persistable;

class KeywordEntity implements Persistable
{

    private $name;

    private $rank;

    private $themeId;


    public function bsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'rank' => $this->getRank(),
            'theme_id' => $this->getThemeId(),
            '__pclass' => new Binary(get_class($this), Binary::TYPE_USER_DEFINED),
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->setName($data['name']);
        $this->setRank($data['rank']);
        $this->setThemeId($data['theme_id']);
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * @param mixed $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return mixed
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * @param mixed $themeId
     */
    public function setThemeId($themeId)
    {
        $this->themeId = $themeId;
    }



}