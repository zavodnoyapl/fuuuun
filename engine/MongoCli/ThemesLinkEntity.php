<?php

namespace MongoCli;

use MongoDB\BSON\Binary;
use MongoDB\BSON\Persistable;

class ThemesLinkEntity implements Persistable
{
    private $themeId;

    private $weight = 0;

    /**
     * @var \DateTime
     */
    private $dateTimeUpdate;

    public function __construct()
    {
        $this->dateTimeUpdate = (new \MongoDB\BSON\UTCDateTime());
    }

    public function bsonUnserialize(array $data)
    {
        $this->setThemeId($data['theme_id']);
        $this->setWeight($data['weight']);
        $this->setDateTimeUpdate($data['date_time_update']);

    }

    public function bsonSerialize()
    {
        return [
            'theme_id' => $this->getThemeId(),
            'weight' => $this->getWeight(),
            'date_time_update' => $this->getDateTimeUpdate(),
            '__pclass' => new Binary(get_class($this), Binary::TYPE_USER_DEFINED),
        ];
    }

    /**
     * @return mixed
     */
    public function getThemeId()
    {
        return $this->themeId;
    }

    /**
     * @param mixed $themeId
     */
    public function setThemeId($themeId)
    {
        $this->themeId = $themeId;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return \DateTime
     */
    public function getDateTimeUpdate()
    {
        return $this->dateTimeUpdate;
    }

    /**
     * @param $dateTimeUpdate
     */
    public function setDateTimeUpdate($dateTimeUpdate)
    {
        $this->dateTimeUpdate = $dateTimeUpdate;
    }



}