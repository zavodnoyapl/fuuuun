<?php

namespace MongoCli;

use MongoDB\BSON\Binary;
use MongoDB\BSON\Persistable;

class ThemesEntity implements Persistable
{
    const NAME = 'theme';

    private $id;

    private $name;

    /**
     * @var ThemesLinkEntity[]
     */
    private $links = [];

    public function __construct()
    {
        $this->id = new \MongoDB\BSON\ObjectId();
    }

    public function bsonUnserialize(array $data)
    {
        $this->id = $data['_id'];
        $this->name = $data['name'];
        $links = [];
        foreach ($data['links'] as $link) {
            $links[] = $link;
        }

        $this->setLinks($links);
    }

    public function bsonSerialize()
    {
        $links = array_map(function (ThemesLinkEntity $elem) {
            return $elem->bsonSerialize();
        }, $this->getLinks());
        return [
            '_id' => $this->id,
            'name' => $this->getName(),
            'links' => $links,
            '__pclass' => new Binary(get_class($this), Binary::TYPE_USER_DEFINED),
        ];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ThemesLinkEntity[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param array $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * @param \MongoDB\BSON\ObjectId $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }




}