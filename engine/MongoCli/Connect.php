<?php
namespace MongoCli;

use MongoDB\Client;

class Connect
{
    private static $db;

    private static $instance;

    public  static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $connect =  new Client();
        self::$db = $connect->selectDatabase('engine');
    }

    /**
     * @param $name
     * @return \MongoDB\Collection
     */
    public function getCollection($name)
    {
        return self::$db->selectCollection($name);
    }


}