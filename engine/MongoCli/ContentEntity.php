<?php

namespace MongoCli;

use MongoDB\BSON\Binary;
use MongoDB\BSON\Persistable;

class ContentEntity implements Persistable
{
    const NAME = 'content';

    private $id;

    private $pageId;

    private $content;

    public function __construct()
    {
        $this->id = new \MongoDB\BSON\ObjectId();
    }

    public function bsonSerialize()
    {
        return [
            '_id' => $this->getId(),
            'page_id' => $this->getPageId(),
            'content' => $this->getContent(),
            '__pclass' => new Binary(get_class($this), Binary::TYPE_USER_DEFINED),
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->id = $data['_id'];
        $this->setPageId($data['page_id']);
        $this->setContent($data['content']);
    }


    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param mixed $pageId
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }




}