<?php

namespace MongoCli;

use MongoDB\BSON\Binary;
use MongoDB\BSON\Persistable;

class PageEntity implements Persistable
{
    const NAME = 'page';

    private $id;

    private $contentId;

    private $url;

    private $title;

    private $pageRank = 0;

    private $isParse = false;

    private $dateAdd;

    /**
     * @var KeywordEntity[]
     */
    private $keywords = [];

    public function __construct()
    {
        $this->dateAdd = new \MongoDB\BSON\UTCDateTime();
        $this->id = new \MongoDB\BSON\ObjectId();
    }

    public function bsonSerialize()
    {
        $keywords = array_map(function (KeywordEntity $elem) {
            return $elem->bsonSerialize();
        }, $this->getKeywords());
        return [
            '_id' => $this->getId(),
            'content_id' => $this->getContentId(),
            'url' => $this->getUrl(),
            'title' => $this->getTitle(),
            'keywords' => $keywords,
            'page_rank' => $this->getPageRank(),
            '__pclass' => new Binary(get_class($this), Binary::TYPE_USER_DEFINED),
            'is_parse' => $this->isIsParse(),
            'date_add' => $this->dateAdd
        ];
    }

    public function bsonUnserialize(array $data)
    {
        $this->id = $data['_id'];
        $this->setContentId($data['content_id']);
        $this->setUrl($data['url']);
        $this->setTitle($data['title']);
        $keywords = [];
        foreach ($data['keywords'] as $keyword) {
            $keywords[] = $keyword;
        }
        $this->setKeywords($keywords);
        $this->setPageRank($data['page_rank']);
        $this->setIsParse($data['is_parse']);
        $this->setDateAdd($data['date_add']);
    }


    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isIsParse()
    {
        return $this->isParse;
    }

    /**
     * @param bool $isParse
     */
    public function setIsParse($isParse)
    {
        $this->isParse = $isParse;
    }



    /**
     * @return mixed
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * @param mixed $contentId
     */
    public function setContentId($contentId)
    {
        $this->contentId = $contentId;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getPageRank()
    {
        return $this->pageRank;
    }

    /**
     * @param int $pageRank
     */
    public function setPageRank($pageRank)
    {
        $this->pageRank = $pageRank;
    }

    /**
     * @return KeywordEntity[]
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param array $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return \MongoDate
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param \MongoDate $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }






}