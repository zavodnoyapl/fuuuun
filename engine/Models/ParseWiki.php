<?php
namespace Models;

use PHPHtmlParser\Dom;
use webd\language\PorterStemmer;

class ParseWiki implements ParseInterface
{
    const URL_PART = 'https://en.wikipedia.org/wiki';
    private $content = '';
    private $title = '';
    private $links = [];
    private $url;
    private $keywords = [];

    public function run($url)
    {
        $dom = new Dom();
        $dom->loadFromUrl($url);
        $this->title = $dom->find('title')[0]->text();

        $content = $dom->find('#mw-content-text')[0];
        $dom->load($content);
        $remove = $dom->find('.reflist')[0];
        $content = mb_convert_encoding($content, 'UTF-8', 'auto');
        $content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $content);
        $content = preg_replace('/<!--(.*)-->|/Uis', '', $content);
        $content = str_replace($remove, '', $content);


        $regexp = "<a\s[^>]*href=(\"??)\/wiki\/*([^\" >]*?)\\1[^>]*>(.*)<\/a>";
        if (preg_match_all("/$regexp/siU", $content, $matches)) {
            $this->links = $matches[2];
        }



        $dataList = $this->transformContent($content);
        $dataListInfinitive = [];
        $all = 0;
        foreach ($dataList as $key => $word) {
            $wordTransform = PorterStemmer::Stem($word);
            if (in_array($wordTransform, ['thi', 'at', 'the', 'or', 'and', 'about', 'for', 'that', 'ar', 'with', 'edit', 'from', 'had', 'him', 'new', 'but', 'thei', 'also', 'their']) || strlen($wordTransform) <= 2) {
                continue;
            }
            $dataListInfinitive[$wordTransform] = isset($dataListInfinitive[$wordTransform]) ? $dataListInfinitive[$wordTransform] + 1 : 1;
            $all++;
        }
        arsort($dataListInfinitive);
        $i = 0;
        $result = [];
        foreach ($dataListInfinitive as $popular => $count) {
            $i++;
            $rate = round((100/$all) * $count, 3);
            if ($i >= 6 || $rate < 0.7) {
                break;
            }

            $result[$popular] = $rate;
        }
        $this->keywords = $result;



    }

    public function transformContent($content)
    {
        $content = preg_replace('/<[^>]*>/', '', $content);

        $content = preg_replace("/(#\d+;)/", '', $content);
        $content = preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/"," ",$content);
        $content = preg_replace("/(\s){2,}/", " ", $content);
        $content = preg_replace('#\b[^\s]{1,2}\b\s*#', '', $content);
        $content = preg_replace('#\b[^\s]{1,2}\b\s*#', '', $content);
        $content = preg_replace ("/[^a-zA-ZА-Яа-я0-9\s]/"," ",$content);
        $content = str_replace(' displaystyle ', ' ', $content);
        $content = mb_strtolower($content);
        $this->content = $content;

        return explode(' ', $content);
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }


   public function getUrl()
   {
       return $this->url;
   }

   public function getKeywords()
   {
       return $this->keywords;
   }
}