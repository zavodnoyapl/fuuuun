<?php
namespace Models;
interface ParseInterface
{
    public function getUrl();

    public function getLinks();

    public function getContent();

    public function getTitle();

    public function getKeywords();
}