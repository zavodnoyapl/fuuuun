<?php
require __DIR__ . '/../vendor/autoload.php';


parse(\Models\ParseWiki::URL_PART . '/Pulp_Fiction');


function parse($urlParse)
{

    $parser = new \Models\ParseWiki();

    $parser->run($urlParse);
    if (!$page = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->findOne(['url' => $urlParse])) {
        $page = new \MongoCli\PageEntity();
    }
    if ($page->isIsParse()) {
        return;
    }
    echo $parser->getTitle() . "\n";
    $keywords = $parser->getKeywords();
    $themeEntityList = [];
    foreach ($keywords as $theme => $rate) {
        if (!$themeEntity = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->findOne(['name' => $theme])) {
            $themeEntity = new \MongoCli\ThemesEntity();
            $themeEntity->setName($theme);
        }
        $themeEntityList[] = $themeEntity;
    }
    $dataThemes = [];
    foreach ($themeEntityList as $themesEntity) {
        $dataThemes[$themesEntity->getName()] = $themesEntity->getId();
    }
    addWeightTheme($themeEntityList);
    $keywordEntityList = [];

    foreach ($keywords as $theme => $rate) {
        $keywordsEntity = new \MongoCli\KeywordEntity();
        $keywordsEntity->setName($theme);
        $keywordsEntity->setRank($rate);
        $keywordsEntity->setThemeId($dataThemes[$theme]);
        $keywordEntityList[] = $keywordsEntity;
    }
    $page->setKeywords($keywordEntityList);
    $page->setIsParse(true);
    $page->setUrl($urlParse);
    $page->setTitle($parser->getTitle());

    $contentEntity = new \MongoCli\ContentEntity();
    $contentEntity->setContent($parser->getContent());
    $contentEntity->setPageId($page->getId());
    $page->setContentId($contentEntity->getId());
    MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->updateOne(['_id' => $page->getId()], ['$set' => $page->bsonSerialize()],['upsert' => true]);
    MongoCli\Connect::getInstance()->getCollection(\MongoCli\ContentEntity::NAME)->insertOne($contentEntity);


    $links = $parser->getLinks();
    foreach ($links as $link) {

        if ($page = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->findOneAndUpdate(['url' => \Models\ParseWiki::URL_PART . $link], ['$inc' => ['page_rank' => 1]])) {
            continue;
        }
        if (preg_match("#\.(jpg|png|gif)#i", $link)) {
            continue;
        }
        $page = new \MongoCli\PageEntity();
        $page->setPageRank(1);
        $page->setUrl(\Models\ParseWiki::URL_PART . $link);
        MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->insertOne($page);
    }
}
$i = 0;

while (true) {
    $i++;
    /**
     * @var $page \MongoCli\PageEntity
     */
    $page  = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->findOne(['is_parse' => false], ['sort' => ['date_add' => 1]]);
    if (!$page || $i > 1000000) {
        die;
    }
    try {
        parse($page->getUrl());
    } catch (\Exception $e) {
        echo $e->getMessage() . "\n";
    }
    if ($i%100) {
        \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['$where' => 'this.links > 1000'], ['$pull' => ['links' => ['weight' => ['$lte' => 1]]]], ['multi' => true]);
    }
}
/**
db.theme.update(
{ },
{ $pull: { links: { weight: {$lte: 1} } } } ,
{ multi: true }
)
 */

/**
 * @param $themeEntityList \MongoCli\ThemesEntity[]
 */
function addWeightTheme($themeEntityList)
{
    foreach ($themeEntityList as &$themesEntityBase) {
        $dataTheme = $themesEntityBase->bsonSerialize();
        \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateOne(['_id' => $themesEntityBase->getId()], ['$set' => $dataTheme], ['upsert' => true]);
        $newLinkList = [];
        foreach ($themeEntityList as $themesEntity) {
            if ($themesEntityBase->getId() == $themesEntity->getId()) {
                continue;
            }
            if (!MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['_id' => $themesEntityBase->getId(), 'links.theme_id' => $themesEntity->getName()], ['$inc' => ["links.$.weight" => 1]])->getModifiedCount()) {
                $newLink = new \MongoCli\ThemesLinkEntity();
                $newLink->setWeight(1);
                $newLink->setThemeId($themesEntity->getName());
                $newLink->setDateTimeUpdate(new DateTime());
                $newLinkList[] = $newLink;
            }
        }
        if ($newLinkList) {
            MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['_id' => $themesEntityBase->getId()], ['$push' => ['links' => ['$each' => $newLinkList]]])->getModifiedCount();
        }

    }
}
