<?php
require __DIR__ . '/../vendor/autoload.php';

\MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->createIndexes(
[
    [ 'key' => [ 'url' => 1 ], 'unique' => true ],
    [ 'key' => [ 'content_id' => 1 ]],
    [ 'key' => [ 'keywords.name' => 1 ]],
    [ 'key' => [ 'keywords.theme_id' => 1 ]],
    [ 'key' => [ 'date_add' => -1 ]],
    [ 'key' => [ 'page_rank' => -1 ]],
    [ 'key' => [ 'keywords.rank' => -1 ]]
]
);

\MongoCli\Connect::getInstance()->getCollection(\MongoCli\ContentEntity::NAME)->createIndexes(
    [
        [ 'key' => [ 'content' => 'text'] ],
        [ 'key' => [ 'page_id' => 1 ]],
    ]
);

\MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->createIndexes(
    [
        [ 'key' => [ 'links.theme_id' => 1] ],
        [ 'key' => [ 'name' => 1 ], 'unique' => true ],
        [ 'key' => [ 'links.weight' => -1 ]]
    ]
);