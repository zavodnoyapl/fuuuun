<?php
require __DIR__ . '/../vendor/autoload.php';

$search = 'Regina King';
$parser = new \Models\ParseWiki();
$data = $parser->transformContent($search);
$dataListInfinitive = [];
foreach ($data as $key => $word) {
    $wordTransform = \webd\language\PorterStemmer::Stem($word);
    if (in_array($wordTransform, ['thi', 'at', 'the', 'or', 'and', 'about', 'for', 'that', 'ar', 'with', 'edit', 'from', 'had', 'him', 'new', 'but', 'thei', 'also', 'their']) || strlen($wordTransform) <= 2) {
        continue;
    }
    $dataListInfinitive[$wordTransform] = isset($dataListInfinitive[$wordTransform]) ? $dataListInfinitive[$wordTransform] + 1 : 1;
}
$data = array_keys($dataListInfinitive);
var_dump($data);

/**
 *
db.theme.aggregate([
{$match: {'name': {$in: ['bbc', 'news']}}},
{$unwind: '$links'},
{
$graphLookup: {
from: 'theme',
startWith: '$links.theme_id',
connectFromField: 'links.theme_id',
connectToField: 'name',
as: 'connections',
depthField: 'steps',
maxDepth: 0,
}
},
{$unwind: '$connections'},
{$sort: {steps: 1}},
{$group: {
_id: '$links.theme_id',
stars: {$sum: '$links.weight'},
connections: {$first: '$connections.name'}
}},
{$match: {'connections': {$nin: ['bbc', 'news']}, 'stars': {'$gt': 1}}},
{$sort: {stars: -1}},

])
 */