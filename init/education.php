<?php
require __DIR__ . '/../vendor/autoload.php';

/**
 * @var $list \MongoCli\PageEntity[]
 */
$list = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\PageEntity::NAME)->find(['is_parse' => true]);
$i = 0;
foreach ($list as $page) {
    $themeEntityList = [];
    $i++;
    foreach ($page->getKeywords() as $keyword) {

        if (!$themeEntity = \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->findOne(['_id' => $keyword->getThemeId()])) {
            $themeEntity = new \MongoCli\ThemesEntity();
            $themeEntity->setName($keyword->getName());
            $themeEntity->setId($keyword->getThemeId());
        }
        $themeEntityList[] = $themeEntity;
    }

    if ($i%100 == 0) {
        echo $i. "\n";
    }
    addWeightTheme($themeEntityList);

    \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['$where' => 'this.links > 5000'], ['$pull' => ['links' => ['weight' => ['$lte' => 1]]]], ['multi' => true]);


}

/**
 * @param $themeEntityList \MongoCli\ThemesEntity[]
 */
function addWeightTheme($themeEntityList)
{
    foreach ($themeEntityList as &$themesEntityBase) {
        $dataTheme = $themesEntityBase->bsonSerialize();
        \MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateOne(['_id' => $themesEntityBase->getId()], ['$set' => $dataTheme], ['upsert' => true]);
        $newLinkList = [];
        foreach ($themeEntityList as $themesEntity) {
            if ($themesEntityBase->getId() == $themesEntity->getId()) {
                continue;
            }
            if (!MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['_id' => $themesEntityBase->getId(), 'links.theme_id' => $themesEntity->getId()], ['$inc' => ["links.$.weight" => 1]])->getModifiedCount()) {
                $newLink = new \MongoCli\ThemesLinkEntity();
                $newLink->setWeight(1);
                $newLink->setThemeId($themesEntity->getId());
                $newLink->setDateTimeUpdate(new DateTime());
                $newLinkList[] = $newLink;
            }
        }
        if ($newLinkList) {
            MongoCli\Connect::getInstance()->getCollection(\MongoCli\ThemesEntity::NAME)->updateMany(['_id' => $themesEntityBase->getId()], ['$push' => ['links' => ['$each' => $newLinkList]]])->getModifiedCount();
        }

    }
}
